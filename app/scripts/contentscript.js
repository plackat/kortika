// inject this code to flip ebay item images larger than 150px

var ImgFlipBoard = function (options) {
    var flipTemplate = '<div class="flip-container"><div class="flipper"><div class="front"></div><div class="back"></div></div></div>';
    var showBackImage = function () {
        $(this).addClass('hover');
    };
    var showFrontImage = function () {
        $(this).removeClass('hover');
    };

    var injectStyles = function () {
        var cssRule = '.flip-container {' +
                'perspective: 1000;' +
                'transform-style: preserve-3d;' +
            '}' +
            '.flip-container:hover .back {' +
                'transform: rotateY(0deg);' +
            '}' +
            '.flip-container:hover .front {' +
                'transform: rotateY(180deg);' +
            '}' +

            '.flipper {' +
                'transition: 0.2s;' +
                'transform-style: preserve-3d;' +
                'position: relative;' +
            '}' +

            /* hide back of pane during swap */
            '.front, .back {' +
                'backface-visibility: hidden;' +
                'transition: 0.6s;' +
                'transform-style: preserve-3d;' +
                'position: absolute;' +
                'top: 0;' +
                'left: 0;' +
            '}' +

            '.front {' +
                'z-index: 2;' +
                'transform: rotateY(0deg);' +
            '}' +

            '.back {' +
                'transform: rotateY(-180deg);' +
            '}' +
             '.backtitle {' +
                'padding:4px;' +
                'margin:10px;' +
                'border:5px solid black' +
            '}' +
            /*
             Some vertical flip updates
             */
            '.vertical.flip-container {' +
                'position: relative;' +
            '}' +

            '.vertical .back {' +
                'transform: rotateX(180deg);' +
            '}' +

            '.vertical.flip-container:hover .back {' +
                'transform: rotateX(0deg);' +
            '}' +

            '.vertical.flip-container:hover .front {' +
                'transform: rotateX(180deg);' +
            '}';

        var div = $("<div />", {
            html: '&shy;<style>' + cssRule + '</style>'
        }).appendTo("body");
    };

    var setImageFlipBoard = function (i, el) {
        var $el = $(el);
        var $fliptemplate = $(flipTemplate);
        var $parent = (options.parentClass) ? $el.parents(options.parentClass) : $el.parent();
        var $itemContext = $el.closest(options.itemParentClass) || $el.closest('li');
        // append flipboard template
        $fliptemplate.appendTo($parent);

        // set front div
        var $frontDiv = $fliptemplate.find('.front');
        $frontDiv.append($el);
        $frontDiv.css({height: el.height, width: el.width});

        // set back div
        var $backDiv = $fliptemplate.find('.back');
        // metaData
        var imgsrc = $el.attr('src');
        var title = $(options.titleClass, $itemContext).text();
        var price = $(options.priceClass, $itemContext).text();
        $thumb = $el.clone();
        $thumb.height(el.height/3);
        $thumb.width(el.width/3);
        $backDiv.append($thumb);
        $backDiv.append('<div class="backtitle">'+title+ ' - ' + price + '</div>');
        $backDiv.css({height: el.height, width: el.width});

        // set template size
        $fliptemplate.css({height: el.height, width: el.width});

        // bind events
        $parent.on('mouseenter', showBackImage);
        $parent.on('mouseleave', showFrontImage);
    };

    // start the magic
    if (options.collection.length > 0) {
        injectStyles();
        $.each(options.collection, setImageFlipBoard);
    }else {
        alert('Sorry your collection is empty');
    }
};

$(function () {
    var options = {
        collection: $('img').filter(function (i, el) {
            if (el.height > 150 && el.width > 150) {
                return el;
            }
        }),
        itemParentClass: 'li ',
        parentClass: '.lvpicinner',
        titleClass: '.lvtitle',
        priceClass:'.lvprice span'
    };
    var ebayflip = new ImgFlipBoard(options);
});